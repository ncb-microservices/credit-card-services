package com.ncb.creditcard.soap.client;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.ncb.creditcard.models.generated.CreditCardDetailsRequest;
import com.ncb.creditcard.models.generated.CreditCardDetailsResponse;
import com.ncb.creditcard.models.generated.CreditCardPaymentRequest;
import com.ncb.creditcard.models.generated.CreditCardPaymentResponse;
import com.ncb.creditcard.models.generated.CreditCardTransactionRequest;
import com.ncb.creditcard.models.generated.CreditCardTransactionResponse;

@Service
public class CallCreditCardService {
	
	@Autowired
	private Environment environment;
	
	@Autowired
	SOAPConnector soapConnector;
	
	public CreditCardDetailsResponse getCreditCardDetails(CreditCardDetailsRequest request) {
		CreditCardDetailsResponse response = new CreditCardDetailsResponse();
		response = (CreditCardDetailsResponse) soapConnector.callWebService(environment.getProperty("ENDPOINT_URL"), request);
		return response;
	}
	
	public CreditCardPaymentResponse payCreditCard(CreditCardPaymentRequest request) {
		CreditCardPaymentResponse response = new CreditCardPaymentResponse();
		response = (CreditCardPaymentResponse) soapConnector.callWebService(environment.getProperty("ENDPOINT_URL"), request);
		
		return response;
	}
	
	public CreditCardTransactionResponse getCreditCardTransactions(CreditCardTransactionRequest request) {
		CreditCardTransactionResponse response = new CreditCardTransactionResponse();
		response = (CreditCardTransactionResponse) soapConnector.callWebService(environment.getProperty("ENDPOINT_URL"), request);
		return response;
	}
	
	public CreditCardDetailsResponse getCreditCardDetails(String ccNumber) {
		CreditCardDetailsRequest request = new CreditCardDetailsRequest();
		request.setCardNumber(ccNumber);
		CreditCardDetailsResponse response = new CreditCardDetailsResponse();
		response = (CreditCardDetailsResponse) soapConnector.callWebService(environment.getProperty("ENDPOINT_URL"), request);
		return response;
	}
	
	public CreditCardPaymentResponse payCreditCard(String ccNumber, BigDecimal amount, String currency,
			String jNumber) {
		CreditCardPaymentRequest request = new CreditCardPaymentRequest();
		request.setCardNumber(ccNumber);
		request.setAmount(amount);
		request.setCurrency(currency);
		request.setJournalNumber(jNumber);
		CreditCardPaymentResponse response = new CreditCardPaymentResponse();
		response = (CreditCardPaymentResponse) soapConnector.callWebService(environment.getProperty("ENDPOINT_URL"), request);
		
		return response;
	}
	
	public CreditCardTransactionResponse getCreditCardTransactions(String ccNumber, int numOfRecords) {
		CreditCardTransactionRequest request = new CreditCardTransactionRequest();
		request.setCardNumber(ccNumber);
		request.setNumberOfRecords(numOfRecords);
		CreditCardTransactionResponse response = new CreditCardTransactionResponse();
		response = (CreditCardTransactionResponse) soapConnector.callWebService(environment.getProperty("ENDPOINT_URL"), request);
		return response;
	}
}
