package com.ncb.creditcard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CreditCardServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreditCardServicesApplication.class, args);
	}

}
