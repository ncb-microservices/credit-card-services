package com.ncb.creditcard.controllers;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ncb.creditcard.models.generated.CreditCardDetailsResponse;
import com.ncb.creditcard.models.generated.CreditCardPaymentResponse;
import com.ncb.creditcard.models.generated.CreditCardTransactionResponse;
import com.ncb.creditcard.soap.client.CallCreditCardService;

@RestController
public class CreditCardRestContorller {

	@Autowired
	private CallCreditCardService callCCServices;

	@RequestMapping("/creditCardDetails")
	public CreditCardDetailsResponse getCreditCardDetails(@RequestParam(value = "creditCardNumber") String ccNumber) {
		CreditCardDetailsResponse response = callCCServices.getCreditCardDetails(ccNumber);
		return response;
	}

	@RequestMapping("/creditCardTransactions")
	public CreditCardTransactionResponse getCreditCardTransactions(
			@RequestParam(value = "creditCardNumber") String ccNumber,
			@RequestParam(value = "numOfRecords", defaultValue = "10") int numOfRecords) {
		CreditCardTransactionResponse response = callCCServices.getCreditCardTransactions(ccNumber, numOfRecords);
		return response;
	}

	@PostMapping("/payCard")
	public CreditCardPaymentResponse payCreditCard(@RequestBody Map<String, Object> input) {
		CreditCardPaymentResponse response = callCCServices.payCreditCard((String) input.get("creditCardNumber"),
				new BigDecimal(String.valueOf(input.get("amount"))), (String) input.get("currency"),
				(String) input.get("journalNumber"));
		return response;
	}
}
